FROM microsoft/dotnet
RUN apt-get update && apt-get install -y python3-pip python3 nano curl cron freetds-dev freetds-bin dnsmasq && pip3 install pymssql boto3 awscli requests
RUN sed -i 's/#user=/user=root/g' /etc/dnsmasq.conf && sed -i 's/#listen-address=/listen-address=127.0.0.1/g' /etc/dnsmasq.conf && sed -i 's/#bind-interfaces/bind-interfaces/g' /etc/dnsmasq.conf && mkdir /app /LogFolder
COPY ./ /app
RUN cd /app/Source && dotnet restore RnD.PlayerData.Service.sln && dotnet build RnD.PlayerData.Service.sln -c Release
EXPOSE 80
WORKDIR /app/
