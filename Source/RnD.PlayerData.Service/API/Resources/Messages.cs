namespace RnD.PlayerData.API.Resources
{
	public static class Messages
	{
		public const string BadParameters = "Your parameters are not correct or don't exist!";
	    public const string JsonBodyNotValid = "Your JSON body is not valid!";
        public const string SomethingWentWrong = "Something went wrong, please contact Server Administrator";
    }
}