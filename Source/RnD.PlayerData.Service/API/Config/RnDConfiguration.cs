﻿namespace RnD.PlayerData.API.Config
{
    using System.IO;
    using System.Security.Cryptography.X509Certificates;

    public class RnDConfiguration
    {
        /// <summary>
        /// Retrieves the certificate used to sign Bearer Tokens
        /// </summary>
        /// <returns></returns>
        public static X509Certificate2 GetCertificate()
        {
            var path = Path.Combine(".", "Certificates", "auth.pfx");
            return new X509Certificate2(path, "1234567890");
        }
    }
}
