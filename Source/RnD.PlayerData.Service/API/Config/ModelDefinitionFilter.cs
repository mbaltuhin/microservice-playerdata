using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;
using Swashbuckle.AspNetCore.Swagger;

namespace RnD.PlayerData.API.Config
{
    public class ModelDefinitionFilter : IDocumentFilter
    {
        private readonly Dictionary<string, string> _jsonDefinitions = new Dictionary<string, string>();

        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            BuildJsonDefinitions(swaggerDoc.Definitions);

            // all POST/PUT endpoints
            var endpointsWithBody = swaggerDoc.Paths.Where(p => p.Value.Post != null || p.Value.Put != null);

            //update the OperationIDs to match the name of the methods and have more meaningful name
            foreach (var path in swaggerDoc.Paths)
            {
                if (path.Value.Post != null && path.Value.Post.Summary != null && path.Value.Post.Summary.Contains('-'))
                    path.Value.Post.OperationId = path.Value.Post.Summary.Substring(0, path.Value.Post.Summary.IndexOf('-')).Trim();

                if (path.Value.Put != null && path.Value.Put.Summary != null && path.Value.Put.Summary.Contains('-'))
                    path.Value.Put.OperationId = path.Value.Put.Summary.Substring(0, path.Value.Put.Summary.IndexOf('-')).Trim();

                if (path.Value.Get != null && path.Value.Get.Summary != null && path.Value.Get.Summary.Contains('-'))
                    path.Value.Get.OperationId = path.Value.Get.Summary.Substring(0, path.Value.Get.Summary.IndexOf('-')).Trim();

                if (path.Value.Delete != null && path.Value.Delete.Summary != null && path.Value.Delete.Summary.Contains('-'))
                    path.Value.Delete.OperationId = path.Value.Delete.Summary.Substring(0, path.Value.Delete.Summary.IndexOf('-')).Trim();

            }

        }

        private void AddDescription(Operation operation)
        {
            var endpointModel = operation.Parameters.OfType<BodyParameter>().SingleOrDefault();

            if (endpointModel != null && endpointModel.Schema != null)
            {
                var modelKey = endpointModel.Schema.Ref.Split('/').Last();
                endpointModel.Description = _jsonDefinitions[modelKey];
            }
        }

        private void BuildJsonDefinitions(IDictionary<string, Schema> modelSchemas)
        {
            foreach (var schemaDefinition in modelSchemas)
            {
                if (!_jsonDefinitions.ContainsKey(schemaDefinition.Key))
                {
                    var jsonKeyValues = schemaDefinition.Value.Properties
                        .Select(p => $"\t\"{p.Key}\" : {GetJsonType(p.Value.Type)}");

                    _jsonDefinitions.Add(
                        schemaDefinition.Key,
                        "{\n" + string.Join(",\n", jsonKeyValues) + "\n}"
                    );
                }
            }
        }

        private string GetJsonType(string schemaType)
        {
            // just map to json default value
            switch (schemaType)
            {
                case "string": return "\"\"";
                case "integer": return "0";
                case "number": return "0";
                case "boolean": return "false";
                case "array": return "[]";
                default: return "{}";
            }
        }
    }
}
