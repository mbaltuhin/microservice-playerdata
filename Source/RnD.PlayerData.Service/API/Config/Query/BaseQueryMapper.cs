﻿using RnD.PlayerData.API.DTOS;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using RnD.PlayerData.Services.Exceptions;

namespace RnD.PlayerData.API.Config.Query
{
    public class BaseQueryMapper : IQueryMapper
    {
        protected BinaryExpression ReturnBinaryFilterType(MemberExpression key, ConstantExpression value, string condition)
        {
            switch (condition)
            {
                case "eq":
                    return Expression.Equal(key, value);
                case "ge":
                    return Expression.GreaterThanOrEqual(key, value);
                case "gt":
                    return Expression.GreaterThan(key, value);
                case "le":
                    return Expression.LessThanOrEqual(key, value);
                case "lt":
                    return Expression.LessThan(key, value);
                case "ne":
                    return Expression.NotEqual(key, value);
                default:
                    throw new NotSupportedExpression("Condition is not supported");
            }
        }

        protected MethodCallExpression ReturnStringFilterType(MemberExpression key, ConstantExpression value, string condition)
        {
            switch (condition)
            {
                case "contains":
                    MethodInfo containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
                    return Expression.Call(key, containsMethod, value);
                case "startswith":
                    MethodInfo startsWithMethod = typeof(string).GetMethod("StartsWith", new[] { typeof(string) });
                    return Expression.Call(key, startsWithMethod, value);
                case "endswith":
                    MethodInfo endsWithMethod = typeof(string).GetMethod("EndsWith", new[] { typeof(string) });
                    return Expression.Call(key, endsWithMethod, value);
                default:
                    throw new NotSupportedExpression("Condition is not supported");
            }
        }

        public ValidQueryModel MapToValidQuery(QueryRequestModel query)
        {
            Sorting validOrderByModel = null;
            List<Filtering> filters = new List<Filtering>();
            if (!string.IsNullOrEmpty(query.OdrerBy))
            {
                var parsedOrderingQuery = query.OdrerBy.Split('.');
                var parsedOrderingQueryObject = string.Empty;
                if (parsedOrderingQuery[parsedOrderingQuery.Length - 1] != "asc" && parsedOrderingQuery[parsedOrderingQuery.Length - 1] != "desc")
                {
                    throw new NotSupportedExpression("The order_by should indicate asc or desc form.");
                }
                parsedOrderingQueryObject = string.Join('.', parsedOrderingQuery, 0, parsedOrderingQuery.Length - 1);

                validOrderByModel = new Sorting
                {
                    OrderBy = parsedOrderingQueryObject,
                    Type = parsedOrderingQuery[parsedOrderingQuery.Length - 1]
                };
            }

            if (!string.IsNullOrEmpty(query.Filter))
            {
                var splitterQueryFilters = query.Filter.Split(',');

                if (splitterQueryFilters.Length < 1)
                {
                    throw new NotSupportedExpression("Filters should by separated by ','.");
                }

                foreach (var splittedQueryFilter in splitterQueryFilters)
                {
                    var keyConditionAndValue = splittedQueryFilter.Split(':');

                    if (keyConditionAndValue.Length < 2)
                    {
                        throw new NotSupportedExpression("Filters should be in the following format: [key]=[operator]:[value]");
                    }
                    var value = keyConditionAndValue[1];
                    var keyAndCondition = keyConditionAndValue[0].Split('=');

                    if (keyAndCondition.Length < 2)
                    {
                        throw new NotSupportedExpression("Filters should be in the following format: [key]=[operator]:[value]");
                    }
                    var key = keyAndCondition[0];
                    var condition = keyAndCondition[1];
                    try
                    {
                        Enum.IsDefined(typeof(ValidOperators), condition);
                    }
                    catch (Exception)
                    {
                        throw new NotSupportedException($"Filter condition: {condition} is not valid");
                    }

                    filters.Add(new Filtering
                    {
                        Condition = condition,
                        Key = key,
                        Value = value
                    });
                }
            }

            var validQuery = new ValidQueryModel
            {
                Paging = new Paging
                {
                    Fields = query.Fields ?? null,
                    Limit = query.Limit ?? 10,
                    Offset = query.Offset ?? default(int)
                },
                Sorting = validOrderByModel,
                Filtering = filters
            };



            return validQuery;
        }
    }
}
