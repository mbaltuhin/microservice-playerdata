﻿using RnD.PlayerData.API.DTOS;

namespace RnD.PlayerData.API.Config.Query
{
    public interface IQueryMapper
    {
        ValidQueryModel MapToValidQuery(QueryRequestModel query);
    }
}
