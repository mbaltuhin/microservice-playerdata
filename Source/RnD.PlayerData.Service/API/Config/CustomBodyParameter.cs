using Swashbuckle.AspNetCore.Swagger;

namespace RnD.PlayerData.API.Config
{
    public class CustomBodyParameter : BodyParameter
    {
        public string Type { get; set; }
    }
}