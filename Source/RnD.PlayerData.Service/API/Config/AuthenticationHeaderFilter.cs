namespace RnD.PlayerData.API.Config
{
    using Microsoft.AspNetCore.Mvc.Authorization;
    using Swashbuckle.AspNetCore.Swagger;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using System.Collections.Generic;
    using System.Linq;
    public class AuthenticationHeaderFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var filters = context.ApiDescription.ActionDescriptor.FilterDescriptors;
            var hasAuthorizeAttribute = filters.Select(filterInfo => filterInfo.Filter)
                .Any(filter => filter is AuthorizeFilter);
            var hasAllowAnonymousAttribute = filters.Select(filterInfo => filterInfo.Filter)
                .Any(filter => filter is IAllowAnonymousFilter);

            if (hasAuthorizeAttribute && !hasAllowAnonymousAttribute)
            {
                if (operation.Parameters == null)
                {
                    operation.Parameters = new List<IParameter>();
                }

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "Authorization",
                    In = "header",
                    Description = "access token",
                    Required = true,
                    Type = "string"
                });

				if (operation.Tags.Contains("Storage") && operation.OperationId == "ApiStorageByAccountIdByKeyPost")
				{
					operation.Consumes.Add("text/plain");
					operation.Parameters.Add(new CustomBodyParameter
					{
						Name = "Body",
						In = "body",
						Description = "String Body",
						Required = true,
						Type = "string"
					});
				}
            }
        }
    }
}
