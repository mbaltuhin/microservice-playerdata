﻿
namespace RnD.PlayerData.API.DTOS
{
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class QueryRequestModel
    {
        /// <summary>
        /// limit - not required, if not passed the default value is set to 10. If passed - should be non negative number, otherwise the default value is set
        /// </summary>
        [FromQuery(Name = "limit")]
        [Range(0, int.MaxValue, ErrorMessage = "Value should be non negative")]
        public int? Limit { get; set; }

        /// <summary>
        /// offset - not required, if not passed the default value is set to 0. If passed - should be non negative number, otherwise the default value is set
        /// </summary>
        [Range(0, int.MaxValue, ErrorMessage ="Value should be non negative")]
        [FromQuery(Name = "offset")]
        public int? Offset { get; set; }

        /// <summary>
        /// fields - provide a list of the fields that will be displayed separated by comma. 
        /// Example: fields=property_name_1,property_name_2. If not passed, no property will be filled.
        /// </summary>
        [FromQuery(Name = "fields")]
        public string Fields { get; set; }

        /// <summary>
        /// filter - provide a list of filters(separated by comma) that will be performed while requesting data from the Db.
        /// Example: filter=[property_name]=[condition]:[value], [property_name]=[condition]:[value]
        /// Allowed conditions: eq, lt, lte, gt, gte, contains, starts_with, ends_with 
        /// </summary>
        [FromQuery(Name = "filter")]
        public string Filter { get; set; }

        /// <summary>
        /// order_by - provide property_name and order by direction
        /// Example: order_by=[property_name].[direction]
        /// Allowed Directions: asc, desc
        /// </summary>
        [FromQuery(Name = "order_by")]
        public string OdrerBy { get; set; }

    }

    public class ValidQueryModel
    {
        public Paging Paging { get; set; }
        public List<Filtering> Filtering { get; set; }
        public Sorting Sorting { get; set; }
    }

    public class Paging
    {
        public string Fields { get; set; }
        public int Limit { get; set; }
        public int Offset { get; set; }
    }

    public class Filtering
    {
        public string Key { get; set; }
        public string Condition { get; set; }
        public string Value { get; set; }
    }

    public class Sorting
    {
        public string OrderBy { get; set; }
        public string Type { get; set; }
    }

    public enum ValidOperators
    {
        Eq,
        Lte,
        Gte,
        Contains,
        Starts_With,
        Ends_With
    }
}
