﻿using Amazon.DynamoDBv2.DocumentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace RnD.PlayerData.API.DTOS
{
    public class GetPlayerResponse
    {
        /// <summary>
        /// Player from the Db, represented as a Dictionary
        /// </summary>
        public JObject Player { get; set; }

        public GetPlayerResponse(Document document)
        {

            var filteredDocument = document;
            foreach (var attribute in document.GetAttributeNames())
            {
                if (attribute == "is_player_deleted" || attribute == "game_id")
                {
                    filteredDocument.Remove(attribute);
                }
            }

            var json = JObject.Parse(filteredDocument.ToJsonPretty());
            string jsonToString = null;

            foreach (var item in json.Properties())
            {
                var childrenTokens = item.Count;
                if (item.Value.HasValues)
                {
                    filteredDocument[item.Name] = "[]";
                }
            }

            Player = JObject.Parse(filteredDocument.ToJsonPretty());
        }
    }
}
