﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace RnD.PlayerData.API.DTOS
{
    public class DeletePlayer
    {
        private string playerId;

        public DeletePlayer(string playerId)
        {
            this.PlayerId = playerId;
        }

        /// <summary>
        /// The Id of the Player requested for deletion
        /// </summary>
        [Required]
        [JsonProperty(PropertyName = "player_id")]
        public string PlayerId
        {
            get => this.playerId;
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.playerId = value.Trim();
                }
                else
                {
                    this.playerId = value.Trim();
                };
            }
        }

        public string ToString()
        {
            return this.playerId.ToString();
        }
    }
}
