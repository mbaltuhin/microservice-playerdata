﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;

namespace RnD.PlayerData.API.DTOS
{
    public class PlayerRequest
    {
        private string playerId;
        private JObject player;

        public PlayerRequest(string playerId, object player)
        {
            this.Player = player;
            this.PlayerId = playerId;
        }

        /// <summary>
        /// The Id of the requested Player
        /// </summary>
        [Required]
        [JsonProperty(PropertyName = "player_id")]
        public string PlayerId
        {
            get { return this.playerId; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.playerId = value.Trim();
                }
                else
                {
                    this.playerId = value;
                }
            }
        }

        /// <summary>
        /// The Player Object
        /// </summary>
        [Required]
        [JsonProperty(PropertyName = "player")]
        public object Player { get; set; }
    }
}
