﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;

namespace RnD.PlayerData.API.DTOS
{
    public class UpdatePlayer
    {
        private JObject player;

        public UpdatePlayer(JObject player)
        {
            this.Player = player;
        }

        /// <summary>
        /// Player Object to be updated
        /// </summary>
        [Required]
        [JsonProperty(PropertyName = "player")]
        public JObject Player { get; set; }
    }
}
