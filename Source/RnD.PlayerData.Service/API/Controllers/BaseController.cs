﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace RnD.PlayerData.API.Controllers
{
    [EnableCors("EnableAllCors")]
    public class BaseController : Controller
    {
        protected ILogger CreateLogger(ILoggerFactory factory, string categoryName)
        {
            return factory.CreateLogger(categoryName);
        }
    }
}
