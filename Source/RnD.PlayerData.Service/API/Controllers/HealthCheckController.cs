using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;


namespace RnD.PlayerData.API.Controllers
{
	/// <summary>
	///	Controller for operational tasks concerning DynamoDb
	/// </summary>
	[Route("api/playerdata")]
	// [Authorize]
	public class HealthCheckController : BaseController
	{
		/// <summary>
		/// GetHealthCheck - Endpoint used for health checks
		/// </summary>
		/// <returns>Returns the name of the controller and the time of the call.</returns>
		[HttpGet]
        [SwaggerResponse(200, Type = typeof(string), Description = "Used to monitor application health")]
        public IEnumerable<string> Get()
		{
			return new string[] { "playerdata Controller", DateTime.UtcNow.ToString() };
		}
	}

}