using Amazon.DynamoDBv2.DocumentModel;
using RnD.PlayerData.API.Interfaces;
using RnD.PlayerData.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RnD.PlayerData.API.Extensions;
using RnD.PlayerData.API.Resources;
using RnD.PlayerData.API.DTOS;
using Newtonsoft.Json;
using RnD.PlayerData.Services.Interfaces;
using RnD.PlayerData.Domain;
using Microsoft.Extensions.Options;
using Amazon.SQS.Model;
using System.Net;
using RnD.PlayerData.Services.Domain.Models;
using Amazon.DynamoDBv2.Model;
using RnD.PlayerData.API.Config.Query;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Text;

namespace RnD.PlayerData.API.Controllers
{
    [Route("api/playerData/profiles")]
    [Authorize]
    public class PlayersController : BaseController
    {
        private readonly IDynamoDbService _dynamoDbService;
        private readonly IMessageQueueService _messageQueueService;
        private readonly IOptions<WorkerAppSettingsModel> _workerAppSettingsModel;
        private readonly ILogger _logger;
        private readonly TableSettingsModel _table;
        private readonly IQueryMapper _queryMapper;

        public PlayersController(IDynamoDbService dynamoDb,
                                 IMessageQueueService messageQueueService,
                                 IOptions<WorkerAppSettingsModel> workerAppSettingsModel,
                                 ILoggerFactory loggerFactory,
                                 IOptions<TableSettingsModel> table,
                                 IQueryMapper queryMapper)
        {
            _logger = base.CreateLogger(loggerFactory, GetType().Namespace);
            _dynamoDbService = dynamoDb;
            _messageQueueService = messageQueueService;
            _workerAppSettingsModel = workerAppSettingsModel;
            _table = table.Value;
            _queryMapper = queryMapper;
        }

        /// <summary>
        /// Sends a request to add new player to DynamoDB table
        /// </summary>
        /// <param name="request">PlayerRequest - object item as property, that contains the data for the new player</param>
        /// <returns>The created object metadata</returns>       
        [HttpPost]
        [Authorize(Policy = "Write")]
        [SwaggerResponse(200, Type = typeof(GetPlayerResponse), Description = "Successfully created player")]
        [SwaggerResponse(400, Description = "Input parameters were missing or invalid")]
        [SwaggerResponse(500, Description = "Internal Server error, not related to the client request")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> CreatePlayer([FromBody] PlayerRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string playerId = null;
            string gameName = null;
            string gameId = null;

            playerId = this.User.GetPlayerId();
            gameName = this.User.GetGameName();
            gameId = this.User.GetGameId();

            if (playerId == null)
            {
                _logger.LogError("Player id is missing!");
                return BadRequest("Player id is missing!");
            }

            if (gameName == null)
            {
                _logger.LogError("Game name is missing!");
                return BadRequest("Game name is missing!");
            }

            if (gameId == null)
            {
                _logger.LogError("Game id is missing!");
                return BadRequest("Game id is missing!");
            }

            string tableName = BuildTableName();

            if (string.IsNullOrEmpty(request.PlayerId))
            {
                _logger.LogError($"Player Id is null or empty! Returning BadRequest...");
                return BadRequest(Messages.BadParameters);
            }

            _logger.LogInformation($"Verifying JSON body.");

            _logger.LogInformation($"Game name detected: {gameName}");

            Document operationResult = null;
            try
            {
                _logger.LogInformation($"Loading connection to DynamoDB table");
                _dynamoDbService.SetProfilesTable(tableName);

                _logger.LogInformation($"Inserting a new item to DynamoDB with primary key - {request.PlayerId}");
                operationResult = await _dynamoDbService.CreatePlayer(request.PlayerId, request.Player.ToString(), tableName, gameId);

                return Ok(new GetPlayerResponse(operationResult));
            }
            catch (MissingRequiredParameterException ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest(ex.Message);
            }
            catch (AmazonClientException ex)
            {
                _logger.LogError($"An Amazon DynamoDB error has occured in creating a client; error: {ex.ToString()}");
                return StatusCode(500, ex);
            }
            catch (AmazonOperationException ex)
            {
                _logger.LogError($"An Amazon DynamoDb error has occured in INSERTING an object; error: {ex.ToString()}");
                return StatusCode(500, ex);
            }
        }


        /// <summary>
        /// Sends a request to update player/s in DynamoDB table
        /// </summary>
        /// <returns>The created object metadata</returns>
        [HttpPost("update")]
        [Authorize(Policy = "Write")]
        [SwaggerResponse(200, Type = typeof(List<GetPlayerResponse>), Description = "Successfully Updated batch players")]
        [SwaggerResponse(400, Description = "Input parameters were missing or invalid")]
        [SwaggerResponse(500, Description = "Internal Server error, not related to the client request")]
        [SwaggerResponse(503, Description = "Amazon Service Unavailable")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> BatchUpdatePlayers([FromBody] List<UpdatePlayer> request)
        {
            string gameName = this.User.GetGameName();

            if (gameName == null)
            {
                _logger.LogError("Game name is missing!");
                return BadRequest("Game name is missing!");
            }

            string tableName = BuildTableName();

            var result = new List<SendMessageBatchResponse>();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _logger.LogInformation($"Verifying JSON body.");
            _logger.LogInformation($"Trying to fetch gameName from access token.");
            _logger.LogInformation($"Game name detected: {gameName}");
            _logger.LogInformation($"Loading connection to DynamoDB table");
            _dynamoDbService.SetProfilesTable(tableName);

            var requestDataAsDocument = new List<UpdateDocument>();
            try
            {
                var queueUrl = await _messageQueueService.GetOrCreateQueueAsync(_workerAppSettingsModel.Value.QueueName);
                List<UpdateDocument> documentsToBeEnqueued = new List<UpdateDocument>();
                foreach (var item in request)
                {
                    dynamic jsonBodyAsJson = JsonConvert.DeserializeObject(item.Player.ToString());
                    var playerId = string.Empty;
                    Document documentItem = _dynamoDbService.TrimInputJson(Convert.ToString(jsonBodyAsJson));

                    if (!documentItem.Keys.Contains(_table.KeyName))
                    {
                        return BadRequest($"All Json Objects should contain {_table.KeyName}");
                    }

                    playerId = documentItem[_table.KeyName];
                    var trimetToJson = documentItem.ToJson();
                    UpdateDocument processedDocument = _dynamoDbService.ProcessUpdate(playerId, trimetToJson, tableName);
                    requestDataAsDocument.Add(processedDocument);

                    if (requestDataAsDocument.Count < 10)
                    {
                        documentsToBeEnqueued.Add(processedDocument);
                    }
                    else
                    {
                        var enqueLargerAmountOfDocuments = await _messageQueueService.Enqueue(documentsToBeEnqueued, queueUrl);
                        documentsToBeEnqueued.Clear();
                        documentsToBeEnqueued.Add(processedDocument);
                        result.Add(enqueLargerAmountOfDocuments);
                    }
                }

                var enqueuSmallerAmountOfDocuments = await _messageQueueService.Enqueue(documentsToBeEnqueued, queueUrl);
                result.Add(enqueuSmallerAmountOfDocuments);
                return Ok(result);
            }
            catch (MissingRequiredParameterException ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest(ex.Message);
            }
            catch (AmazonSQSEnqueueException ex)
            {
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }

        /// <summary>
        /// Sends a request to delete player with the provided id in the JSON body
        /// </summary>
        /// <param name="player">item that holds the id of the player that should be deleted</param>
        /// <returns>Returns data for the deleted player</returns>       
        [HttpDelete]
        [Authorize(Policy = "Delete")]
        [SwaggerResponse(200, Type = typeof(GetPlayerResponse), Description = "Successfully deleted player")]
        [SwaggerResponse(400, Description = "Input parameters were missing or invalid")]
        [SwaggerResponse(404, Description = "Player with the provided Id not found")]
        [SwaggerResponse(500, Description = "Internal Server error, not related to the client request")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> DeletePlayer([FromBody] DeletePlayer player)
        {
            _logger.LogInformation($"Verifying parameters: playerId - {player.PlayerId}");

            if (string.IsNullOrEmpty(player.PlayerId))
            {
                _logger.LogError($"Player Id is null or empty! Returning BadRequest...");
                return BadRequest(Messages.BadParameters);
            }

            _logger.LogInformation($"Trying to fetch game name from authorization token.");

            string gameName = this.User.GetGameName();

            if (gameName == null)
            {
                _logger.LogError("Game name is missing!");
                return BadRequest("Game name is missing!");
            }

            string tableName = BuildTableName();

            _logger.LogInformation($"Game name: {tableName}");
            _logger.LogInformation($"Loading connection to DynamoDB table");
            _dynamoDbService.SetProfilesTable(tableName);

            try
            {
                _logger.LogInformation($"Trying to delete a DynamoDB item with primary key - {player.PlayerId}");
                var result = await _dynamoDbService.DeletePlayer(tableName, player.ToString());
                if (result == null)
                {
                    _logger.LogError($"DynamoDB item with primary key {player.PlayerId} was NOT found!");
                    return StatusCode(404, $"Player with id {player.PlayerId} is not found!");
                }
                return Ok((result));
            }
            catch (ItemNotFoundException ex)
            {
                _logger.LogError($"Player with id {player.PlayerId} not found.");
                return StatusCode(500, ex.Message);
            }
            catch (ConditionalCheckFailedException ex)
            {
                _logger.LogError($"Player with id {player.PlayerId} not found.");
                return StatusCode(500, ex);
            }
            catch (AmazonClientException ex)
            {
                _logger.LogError($"An Amazon DynamoDB error has occured in creating a client; error: {ex.ToString()}");
                return StatusCode(500, ex);
            }
            catch (AmazonOperationException ex)
            {
                _logger.LogError($"An Amazon DynamoDb error has occured in DELETE an object; error: {ex.ToString()}");
                return StatusCode(500, ex);
            }
        }

        /// <summary>
        /// Gets players by the query criterias in the url.            
        /// </summary>
        /// <param name="query"></param>       
        /// <returns>Returns the DynamoDB player in JSON format</returns>
        [HttpGet]
        [Authorize(Policy = "Read")]
        [SwaggerResponse(200, Type = typeof(List<GetPlayerResponse>), Description = "Players successfully retrieved")]
        [SwaggerResponse(400, Description = "Input parameters were missing or invalid")]
        [SwaggerResponse(404, Description = "No Players with the provided criteria were found")]
        [SwaggerResponse(500, Description = "Internal Server error, not related to the client request")]
        [Produces("application/json")]
        public async Task<IActionResult> GetPlayer(QueryRequestModel query)
        {
            try
            {
                var validQueries = _queryMapper.MapToValidQuery(query);
                ScanOperationConfig config = new ScanOperationConfig();
                ScanFilter scanFilter = new ScanFilter();

                foreach (var q in validQueries.Filtering)
                {
                    if (Validation.OperatorMap.ContainsKey(q.Condition))
                    {
                        bool isInt = Int32.TryParse(q.Value, out int intValue);
                        bool isDouble = Double.TryParse(q.Value, out double doubleValue);
                        if (isInt)
                        {
                            scanFilter.AddCondition(q.Key, Validation.OperatorMap[q.Condition], Int32.Parse(q.Value));
                        }
                        else if (isDouble)
                        {
                            scanFilter.AddCondition(q.Key, Validation.OperatorMap[q.Condition], double.Parse(q.Value));
                        }
                        else
                        {
                            scanFilter.AddCondition(q.Key, Validation.OperatorMap[q.Condition], q.Value);
                        }
                    }
                    else
                    {
                        return BadRequest($"Condition {q.Condition} is not valid!");
                    }
                }
                //Add condition to search only for players that are not marked as deleted
                scanFilter.AddCondition(_table.DeletePlayerFlag, ScanOperator.Equal, false);
                //Add condition to check if the game id is the same like the one from the token
                scanFilter.AddCondition(_table.GameId, ScanOperator.Equal, this.User.GetGameId());

                config.Filter = scanFilter;
                config.Limit = validQueries.Paging.Limit;
                config.ConsistentRead = true;

                if (validQueries.Paging.Fields == null)
                {
                    config.Select = SelectValues.AllAttributes;
                }
                else
                {
                    config.AttributesToGet = validQueries.Paging.Fields.Split(",").ToList();
                    config.Select = SelectValues.SpecificAttributes;
                }

                _logger.LogInformation($"Trying to get data for players...");

                string gameName = this.User.GetGameName();

                if (gameName == null)
                {
                    _logger.LogError("Game name is missing!");
                    return BadRequest("Game name is missing!");
                }

                string tableName = BuildTableName();

                var response = await _dynamoDbService.GetPlayers(tableName, config);

                if (response.Count == 0)
                {
                    _logger.LogError($"DynamoDB items were NOT found!");
                    return NotFound();
                }
                _logger.LogInformation($"DynamoDB items located successfully.");

                var mappedResult = new List<GetPlayerResponse>();
                response.ForEach(src =>
                {
                    mappedResult.Add(new GetPlayerResponse(src));

                });

                return Ok(mappedResult);
            }
            catch (AmazonDynamoValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (AmazonClientException ex)
            {
                _logger.LogError($"An Amazon DynamoDB error has occured in creating a client; error: {ex.ToString()}");
                return StatusCode(500, ex);
            }
            catch (AmazonOperationException ex)
            {
                _logger.LogError($"An Amazon DynamoDb error has occured in getting an object; error: {ex.ToString()}");
                return StatusCode(500, ex.Message);
            }
        }


        private string BuildTableName()
        {
            string prefix = _table.Prefix;
            string environment = _table.Environment;
            string sufix = _table.Sufix;

            var table = new StringBuilder();
            table.Append(prefix);
            table.Append(".");
            table.Append(environment);
            table.Append(".");
            table.Append(sufix);

            return table.ToString();
        }
    }

}
