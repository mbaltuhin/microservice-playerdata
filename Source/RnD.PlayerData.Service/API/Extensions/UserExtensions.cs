using RnD.PlayerData.Services.Exceptions;
using System;
using System.Security.Claims;

namespace RnD.PlayerData.API.Extensions
{
    public static class UserExtensions
    {
        public static string GetGameSpace(this ClaimsPrincipal user)
        {
            try
            {
                string gameName = user.FindFirst("game_name")?.Value;

                return gameName;
            }
            catch (Exception)
            {
                throw new InvalidUserPermissionRequest($"User: {GetAccountId(user)} is not eligible for this operation. Please login as Player.");
            }
        }

        public static string GetAccountId(this ClaimsPrincipal user)
        {
            return user.FindFirst("account_id")?.Value;
        }

        public static string GetPlatform(this ClaimsPrincipal user)
        {
            return user.FindFirst("platform")?.Value;
        }



        public static string GetPlayerId(this ClaimsPrincipal user)
        {
            return user.FindFirst("player_id")?.Value;
        }

        public static string GetGameId(this ClaimsPrincipal game)
        {
            return game.FindFirst("game_id")?.Value;
        }

        public static string GetGameName(this ClaimsPrincipal user)
        {
            try
            {
                string gameName = user.FindFirst("game_name")?.Value;

                return gameName;
            }
            catch (Exception)
            {
                throw new InvalidUserPermissionRequest($"User: {GetPlayerId(user)} is not eligible for this operation. Please login as Player.");
            }
        }


    }
}