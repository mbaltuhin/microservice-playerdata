﻿using Amazon.DynamoDBv2.DocumentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RnD.PlayerData.API.Extensions
{
    public static class Validation
    {
        public static Dictionary<string, ScanOperator> OperatorMap = new Dictionary<string, ScanOperator>()
        {
            {"eq", ScanOperator.Equal},
            {"gt", ScanOperator.GreaterThan},
            {"lt", ScanOperator.LessThan},
            {"lte", ScanOperator.LessThanOrEqual},
            {"gte", ScanOperator.GreaterThanOrEqual},
            {"contains", ScanOperator.Contains},
            {"starts_with", ScanOperator.BeginsWith}
        };

        public static string ToSnakeCase(this string str)
        {
            return string.Concat(str.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x.ToString() : x.ToString())).ToLower();
        }

        public static string ToPascalCase(this string str)
        {
            return str.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries).Select(s => char.ToUpperInvariant(s[0]) + s.Substring(1, s.Length - 1)).Aggregate(string.Empty, (s1, s2) => s1 + s2);
        }
    }
}
