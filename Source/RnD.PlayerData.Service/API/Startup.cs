﻿using RnD.PlayerData.API.Config;
using RnD.PlayerData.API.Interfaces;
using RnD.PlayerData.API.Resources;
using RnD.PlayerData.API.Services;
using RnD.PlayerData.Services.Interfaces;
using RnD.PlayerData.Services.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using NLog;
using NLog.Extensions.Logging;
using NLog.Web;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;
using System.Net;
using RnD.PlayerData.Domain;
using Amazon.DynamoDBv2;
using Amazon.S3;
using Amazon.SQS;
using System;
using RnD.PlayerData.Services.Domain.Models;
using RnD.PlayerData.API.Extensions;
using RnD.PlayerData.API.Config.Query;

namespace RnD.PlayerData.API
{
	public class Startup
	{
		public Startup(IHostingEnvironment env)
		{
			var builder = new ConfigurationBuilder()
				 .SetBasePath(env.ContentRootPath)
				 .AddJsonFile("appsettings.json", optional: true, reloadOnChange: false)
				 .AddEnvironmentVariables();

			Configuration = builder.Build();
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			//Table settings
			services.Configure<TableSettingsModel>(Configuration.GetSection("Table"));

			// App settings
			services.Configure<WorkerAppSettingsModel>(Configuration.GetSection("Worker"));
			services.Configure<SettingsModels>(Configuration.GetSection("rnd"));

			// AWS Services
			services.AddDefaultAWSOptions(Configuration.GetAWSOptions());
			services.AddAWSService<IAmazonDynamoDB>();
			services.AddAWSService<IAmazonS3>();
			services.AddAWSService<IAmazonSQS>();

			services.AddAuthentication(o =>
			{
				o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			})
			.AddJwtBearer(o =>
			{
				o.RequireHttpsMetadata = false;
				o.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuerSigningKey = true,
					ValidateIssuer = true,
					ValidIssuer = Configuration["ResourceStrings:ValidIssuer"],
					IssuerSigningKey = new X509SecurityKey(RnDConfiguration.GetCertificate()),
					ValidateAudience = false
				};
			});

			services.AddAuthorization(options =>
			{
				options.AddPolicy("Read", policy => policy.RequireClaim("permissions", Configuration["ResourceStrings:ValidAudienceRead"]));
				options.AddPolicy("Write", policy => policy.RequireClaim("permissions", Configuration["ResourceStrings:ValidAudienceWrite"]));
				options.AddPolicy("Delete", policy => policy.RequireClaim("permissions", Configuration["ResourceStrings:ValidAudienceDelete"]));
				options.AddPolicy("Services", policy => policy.RequireClaim("permissions", Configuration["ResourceStrings:ValidAudienceServices"]));
			});
            services.AddCors(o => o.AddPolicy("EnableAllCors", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddMvc();
            //services.AddMvc(o => o.InputFormatters.Insert(0, new RawRequestBodyFormatter()));
			
			services.AddScoped<IDynamoDbService, DynamoDbService>();
			services.AddScoped<IMessageQueueService, MessageQueueService>();
			services.AddScoped<IHostedService, WorkerService>();
			services.AddScoped<IHostedService, WorkerService>();
            services.AddScoped<IQueryMapper, BaseQueryMapper>();

            var enableSwagger = Convert.ToBoolean(Configuration["Swagger:EnableSwagger"]);
			if (enableSwagger)
			{
				services.AddSwaggerGen(c =>
				{
					c.SwaggerDoc("v1", new Info { Title = "PlayerData Service API", Version = "v1" });

					// Add ability to upload file directly from Swagger
					c.OperationFilter<FormFileOperationFilter>();
					c.OperationFilter<AuthenticationHeaderFilter>();
					c.DocumentFilter<ModelDefinitionFilter>();
					// Set the comments path for the Swagger JSON and UI.
					var basePath = PlatformServices.Default.Application.ApplicationBasePath;
					var xmlPath = Path.Combine(basePath, "RnD.PlayerData.API.xml");
					c.IncludeXmlComments(xmlPath);
				});
			}

			services.Configure<FormOptions>(x =>
			{
				x.ValueLengthLimit = int.MaxValue;
				x.MultipartBodyLengthLimit = int.MaxValue; // In case of multipart
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
            app.UseCors("EnableAllCors");
            if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			var enableSwagger = Convert.ToBoolean(Configuration["Swagger:EnableSwagger"]);
			if (enableSwagger)
			{
				app.UseSwagger(c =>
				{
					c.RouteTemplate = "api/playerData/swagger/{documentName}/swagger.json";
					c.PreSerializeFilters.Add((swaggerDoc, httpReq) => swaggerDoc.BasePath = httpReq.Host.Value);
				});

				app.UseSwaggerUI(c =>
				{
					c.SwaggerEndpoint("/api/playerData/swagger/v1/swagger.json", "Data Storage Service API V1");
					c.RoutePrefix = "api/playerData/swagger";
				});
			}

			loggerFactory.AddConsole(Configuration.GetSection("Logging"));
			loggerFactory.AddDebug();

			loggerFactory.AddNLog();
			var _logger = loggerFactory.CreateLogger(GetType().Namespace);
			app.AddNLogWeb();

			env.ConfigureNLog("Config//nlog.config").Reload();

			app.UseExceptionHandler(
			   options =>
			   {
				   options.Run(
					   async context =>
					   {
						   context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
						   context.Response.ContentType = "text/plain";
						   var ex = context.Features.Get<IExceptionHandlerFeature>();
						   _logger.LogCritical(ex.Error.ToString());

						   var err = Messages.SomethingWentWrong;
						   await context.Response.WriteAsync(err).ConfigureAwait(false);
					   });
			   }
		   );

			var p = env.ContentRootPath;
			string newPath = Path.GetFullPath(Path.Combine(p, @"../../../../"));
			var logPath = Path.GetFullPath(Path.Combine(newPath, @"LogFolder"));
			LogManager.Configuration.Variables["logDir"] = logPath;

			app.UseAuthentication();
			app.UseMvc();
		}
	}
}
