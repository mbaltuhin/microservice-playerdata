﻿using System;

namespace RnD.PlayerData.Services.Exceptions
{
    public class AmazonDynamoValidationException : Exception
    {
        public AmazonDynamoValidationException()
        {
        }

        public AmazonDynamoValidationException(string message) : base(message)
        {
        }

        public AmazonDynamoValidationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
