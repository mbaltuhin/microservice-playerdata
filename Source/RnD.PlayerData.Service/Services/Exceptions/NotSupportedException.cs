﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RnD.PlayerData.Services.Exceptions
{
    public class NotSupportedExpression : Exception
    {
        public NotSupportedExpression(string message) : base(message)
        {
        }
    }
}
