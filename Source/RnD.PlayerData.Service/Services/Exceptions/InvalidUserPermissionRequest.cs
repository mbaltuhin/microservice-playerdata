﻿using System;

namespace RnD.PlayerData.Services.Exceptions
{
    public class InvalidUserPermissionRequest : Exception
    {
        public InvalidUserPermissionRequest()
        {
        }

        public InvalidUserPermissionRequest(string message) : base(message)
        {
        }

        public InvalidUserPermissionRequest(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
