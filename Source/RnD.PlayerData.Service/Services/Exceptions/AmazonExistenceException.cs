﻿using System;

namespace RnD.PlayerData.Services.Exceptions
{
    public class AmazonExistenceException : Exception
    {
        public AmazonExistenceException()
        {

        }

        public AmazonExistenceException(string message)
        : base(message)
        {

        }

        public AmazonExistenceException(string message, Exception inner)
        : base(message, inner)
        {

        }
    }
}
