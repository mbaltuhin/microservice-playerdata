﻿using System;

namespace RnD.PlayerData.Services.Exceptions
{
    public class AmazonCreateQueueException : Exception
    {
        public AmazonCreateQueueException()
        {
        }

        public AmazonCreateQueueException(string message) : base(message)
        {
        }

        public AmazonCreateQueueException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
