﻿using System;

namespace RnD.PlayerData.Services.Exceptions
{
    public class AmazonSQSDequeueException : Exception
    {
        public AmazonSQSDequeueException()
        {
        }

        public AmazonSQSDequeueException(string message) : base(message)
        {
        }

        public AmazonSQSDequeueException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
