﻿using System;

namespace RnD.PlayerData.Services.Exceptions
{
    public class AmazonClientException : Exception
    {
        public AmazonClientException()
        {

        }

        public AmazonClientException(string message)
        : base(message)
        {

        }

        public AmazonClientException(string message, Exception inner)
        : base(message, inner)
        {

        }
    }
}
