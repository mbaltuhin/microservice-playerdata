﻿using System;

namespace RnD.PlayerData.Services.Exceptions
{
    public class HttpErrorResponseException :Exception
    {
        public HttpErrorResponseException()
        {

        }

        public HttpErrorResponseException(string message)
            :base(message)
        {

        }

        public HttpErrorResponseException(string message, Exception inner)
            :base(message, inner)
        {

        }
    }
}
