﻿using System;

namespace RnD.PlayerData.Services.Exceptions
{
    public class AmazonSQSEnqueueException : Exception
    {
        public AmazonSQSEnqueueException()
        {
        }

        public AmazonSQSEnqueueException(string message) : base(message)
        {
        }

        public AmazonSQSEnqueueException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
