﻿using System;

namespace RnD.PlayerData.Services.Exceptions
{
    public class MissingRequiredParameterException : Exception
    {
        public MissingRequiredParameterException()
        {
        }

        public MissingRequiredParameterException(string message) : base(message)
        {
        }

        public MissingRequiredParameterException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
