﻿namespace RnD.PlayerData.Services.Exceptions
{
    using System;

    public class DecryptStringException : Exception
    {
        public DecryptStringException()
        {
        }

        public DecryptStringException(string message) : base(message)
        {
        }

        public DecryptStringException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
