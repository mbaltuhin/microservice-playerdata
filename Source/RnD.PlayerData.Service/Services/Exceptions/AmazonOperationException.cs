﻿using System;

namespace RnD.PlayerData.Services.Exceptions
{
    public class AmazonOperationException : Exception
    {
        public AmazonOperationException()
        {

        }

        public AmazonOperationException(string message)
        : base(message)
        {

        }

        public AmazonOperationException(string message, Exception inner)
        : base(message, inner)
        {

        }
    }
}
