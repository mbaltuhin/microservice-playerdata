﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RnD.PlayerData.Services.Common
{
    public class Messages
    {
        public const string ItemNotFound = "{0} with id: {1} not found.";

        public const string ErrorCreatingAmazonQueue = "Error creating Amazon Queue with name: {0}";
        public const string ErrorEnqueueBatchMessages = "Error enqueue Batch Messages";
        public const string ErrorDequeueMessage = "Error dequeue Messages from SQS, the message might be sent more than once";

        public const string MessageDequeued = "Message: {0} successfully dequeued";
    }
}
