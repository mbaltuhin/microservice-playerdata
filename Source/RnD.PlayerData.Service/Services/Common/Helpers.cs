﻿using System;
namespace RnD.PlayerData.Services.Common
{
    public class Helpers
    {
        public static string FileNameBuilder(string playerId, string key)
        {
            // Current naming structure for the file name is:
            // playerId-key
            return string.Join("-", playerId, key);
        }

        public static long ConvertToUnixTimestamp(DateTime dateTime)
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (long)(dateTime - sTime).TotalSeconds;
        }

        public static string BucketNameBuilder(string prefix, string region, string env, string gamespace)
		{
			return string.Join(".", prefix, region, env, gamespace);
		}
    }
}
