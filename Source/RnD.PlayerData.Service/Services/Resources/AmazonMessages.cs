namespace RnD.PlayerData.Services.Resources
{
	public static class AmazonMessages
	{
		public const string AmazonS3ClientException = "Amazon S3 Client Exception";
		public const string AmazonDynamoDbClientException = "Amazon DynamoDb Client Exception";
		public const string NoFileThatContainsKey = "There's no filename that contains";
		public const string ErrorBucketExistence = "Amazon error in CHECK bucket existence";
		public const string ErrorGetListOperation = "Amazon error in GET list operation";
		public const string ErrorPutListOperation = "Amazon error in PUT operation";
		public const string ErrorGetOperation = "Amazon error in GET operation";
		public const string ErrorDeleteOperation = "Amazon error in DELETE operation";
		public const string ErrorGetTableOpearation = "Amazon error in GET table operation";
		public const string ErrorCreateItemOpearation = "Amazon error in CREATE Item operation";
        public const string ErrorOverrrideItemOpearation = "Amazon error in OVERRIDE Item operation";
        public const string ErrorUpdateItemOpearation = "Amazon error in UPDATE Item operation";
		public const string ErrorDeleteItemOpearation = "Amazon error in DELETE Item operation";
		public const string ErrorCreateTable = "Amazon error in CREATE TABLE operation";
	}
}