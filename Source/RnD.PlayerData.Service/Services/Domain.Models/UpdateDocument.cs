﻿
namespace RnD.PlayerData.Domain
{
    public class UpdateDocument
    {
        public string Document { get; set; }

        public string GameSpace { get; set; }
    }
}
