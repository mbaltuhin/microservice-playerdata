﻿namespace RnD.PlayerData.Domain
{
    public class WorkerAppSettingsModel
    {
        public string QueueName { get; set; }

        public double TaskDelayInSeconds { get; set; }

        public int QueueMaxMessages { get; set; }

        public int QueueVisibilityTimeoutInSeconds { get; set; }
    }
}
