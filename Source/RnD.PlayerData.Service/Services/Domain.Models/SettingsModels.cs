namespace RnD.PlayerData.Domain
{
    public class SettingsModels
    {
        public string BucketNamePrefix { get; set; }

        public string BucketRegion { get; set; }

        public string Environment { get; set; }

        public string DynamoDbTablePrefix { get; set; }

		public string PlayerId { get; set; }
    }
}
