﻿
namespace RnD.PlayerData.Services.Domain.Models
{
    public class TableSettingsModel
    {
      //  public string TableName { get; set; }
        public string KeyName { get; set; }
        public string DeletePlayerFlag { get; set; }
        public string GameId { get; set; }
        public string Prefix { get; set; }
        public string Sufix { get; set; }
        public string Environment { get; set; }
    }
}
