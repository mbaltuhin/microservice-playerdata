using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using RnD.PlayerData.Domain;
using RnD.PlayerData.Services.Domain;
using RnD.PlayerData.Services.Domain.Models;

namespace RnD.PlayerData.API.Interfaces
{
    public interface IDynamoDbService
    {     
        Task<List<Document>> GetPlayers(string tableName, ScanOperationConfig config);
        Task<List<Document>> GetPlayers(List<string> playerIds, string gameSpace, string filter);
        Task<Document> CreatePlayer(string playerId, string jsonString, string tableName, string gameId);
        UpdateDocument ProcessUpdate(string playerId, string item, string gamespace);
        Task<Document> UpdateWithConfiguration(UpdateDocument updateDocument);       
        Task<string> DeletePlayer(string tableName, string playerID);       
        void SetProfilesTable(string gameSpace);
        Document TrimInputJson(string jsonString);
    }
}
