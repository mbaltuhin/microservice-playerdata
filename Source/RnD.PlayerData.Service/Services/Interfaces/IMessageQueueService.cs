﻿using Amazon.SQS.Model;
using RnD.PlayerData.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RnD.PlayerData.Services.Interfaces
{
    public interface IMessageQueueService
    {
        Task<SendMessageBatchResponse> Enqueue(List<UpdateDocument> updateDocuments, string queueUrl);

        Task Dequeue(string queueUrl);

        Task<string> GetOrCreateQueueAsync(string queueName);
    }
}
