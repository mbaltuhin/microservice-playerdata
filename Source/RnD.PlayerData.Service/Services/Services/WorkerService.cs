﻿using RnD.PlayerData.Services.Common;
using RnD.PlayerData.Services.Exceptions;
using RnD.PlayerData.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using RnD.PlayerData.Domain;

namespace RnD.PlayerData.Services.Services
{
    public class WorkerService : WorkerBaseService
    {
        private readonly IMessageQueueService _messageQueueService;
		private WorkerAppSettingsModel _settings { get; set;}
        private readonly ILogger _logger;

        public WorkerService(IMessageQueueService messageQueueService, ILoggerFactory loggerFactory, IHostingEnvironment env,IOptions<WorkerAppSettingsModel> settings)
        {
			_settings = settings.Value;
            _messageQueueService = messageQueueService;
            _logger = GetLogger(loggerFactory);
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var queueName = _settings.QueueName;
            var taskDelayInSeconds = _settings.TaskDelayInSeconds;
            try
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    await _messageQueueService.Dequeue(queueName);

                    await Task.Delay(TimeSpan.FromSeconds(Convert.ToDouble(taskDelayInSeconds)), cancellationToken);
                }
            }
            catch (AmazonSQSDequeueException ex)
            {
                _logger.LogCritical(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex.ToString());
            }
        }
    }
}
