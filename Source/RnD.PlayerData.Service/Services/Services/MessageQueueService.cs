﻿using Amazon.SQS;
using Amazon.SQS.Model;
using RnD.PlayerData.API.Interfaces;
using RnD.PlayerData.Domain;
using RnD.PlayerData.Services.Common;
using RnD.PlayerData.Services.Exceptions;
using RnD.PlayerData.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace RnD.PlayerData.Services.Services
{
    public class MessageQueueService : BaseService, IMessageQueueService
    {
        private static IConfigurationRoot Configuration { get; set; }
		private WorkerAppSettingsModel _settings { get; set;}
        private IDynamoDbService _dynamoDbService;
        private readonly ILogger _logger;
        private IAmazonSQS _sqsClient { get; set; }

        public MessageQueueService(ILoggerFactory loggerFactory, IDynamoDbService dynamoDbService, IHostingEnvironment env, IOptions<WorkerAppSettingsModel> settings, IAmazonSQS sqsClient)
        {
			_settings = settings.Value;
            _logger = GetLogger(loggerFactory);
            _dynamoDbService = dynamoDbService;
            _sqsClient = sqsClient;
        }

        public async Task<string> GetOrCreateQueueAsync(string queueName)
        {
            try
            {
                var alreadyExistingQueue = await _sqsClient.ListQueuesAsync(new ListQueuesRequest()
                {
                    QueueNamePrefix = queueName
                });
                if (alreadyExistingQueue.QueueUrls.Any())
                {
                    return alreadyExistingQueue.QueueUrls.First();
                }

                var result = await _sqsClient.CreateQueueAsync(queueName);
                return result.QueueUrl;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw new AmazonCreateQueueException(string.Format(Messages.ErrorCreatingAmazonQueue, queueName));
            }
        }

        public async Task Dequeue(string queueName)
        {
			string queueUrl = string.Empty;
			Message currentSrc = new Message();

            try
            {
                int.TryParse(_settings.QueueMaxMessages.ToString(), out int queueMaxMessages);
                int.TryParse(_settings.QueueVisibilityTimeoutInSeconds.ToString(), out int queueVisibilityTimeout);

                queueUrl = (await GetOrCreateQueueAsync(queueName));

                var messages = await _sqsClient.ReceiveMessageAsync(new ReceiveMessageRequest()
                {
                    MaxNumberOfMessages = (queueMaxMessages == 0) ? 1 : queueMaxMessages,
                    QueueUrl = queueUrl,
                    VisibilityTimeout = (queueVisibilityTimeout == 0) ? 120 : queueVisibilityTimeout
                });

				// You can't catch exceptions raised inside an async foreach
				foreach (var src in messages.Messages)
				{
					currentSrc = src;
                    var updateDocument = JsonConvert.DeserializeObject<UpdateDocument>(src.Body);

                    var result = await _dynamoDbService.UpdateWithConfiguration(updateDocument);

                    var delMsgRes = await _sqsClient.DeleteMessageAsync(new DeleteMessageRequest()
                    {
                        QueueUrl = queueUrl,
                        ReceiptHandle = src.ReceiptHandle
                    });

                    _logger.LogInformation(string.Format(Messages.MessageDequeued, updateDocument.Document));
				}
            }
            catch (Exception ex)
            {
				await _sqsClient.DeleteMessageAsync(new DeleteMessageRequest()
				{
					QueueUrl = queueUrl,
					ReceiptHandle = currentSrc.ReceiptHandle
				});

                _logger.LogError(ex.ToString());
                throw new AmazonSQSDequeueException(Messages.ErrorDequeueMessage);
            }
        }

        public async Task<SendMessageBatchResponse> Enqueue(List<UpdateDocument> documents, string queueUrl)
        {
            try
            {
                var request = new SendMessageBatchRequest()
                {
                    Entries = new List<SendMessageBatchRequestEntry>(),
                    QueueUrl = queueUrl
                };
                documents.ForEach(src =>
                {
                    var entry = new SendMessageBatchRequestEntry()
                    {
                        Id = Guid.NewGuid().ToString(),
                        MessageBody = JsonConvert.SerializeObject(src)
                    };
                    request.Entries.Add(entry);
                });

                var response = await _sqsClient.SendMessageBatchAsync(request);

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw new AmazonSQSEnqueueException(Messages.ErrorEnqueueBatchMessages);
            }
        }
    }
}
