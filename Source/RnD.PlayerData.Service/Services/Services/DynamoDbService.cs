using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using RnD.PlayerData.API.Interfaces;
using RnD.PlayerData.Services.Exceptions;
using RnD.PlayerData.Services.Services;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using RnD.PlayerData.Services.Resources;
using Amazon.DynamoDBv2.Model;
using System.Linq;
using RnD.PlayerData.Services.Common;
using RnD.PlayerData.Domain;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Concurrent;
using Microsoft.Extensions.Options;
using RnD.PlayerData.Services.Domain.Models;
using System.Text;
using RnD.PlayerData.Services.Domain;

namespace RnD.PlayerData.API.Services
{
    public class DynamoDbService : BaseService, IDynamoDbService
    {
        private SettingsModels _settings { get; set; }
        private readonly ILogger _logger;
        private IAmazonDynamoDB _dynamoDbClient { get; set; }
        private ConcurrentDictionary<string, Table> ProfileTables { get; set; }
        private TableSettingsModel _table { get; set; }


        public DynamoDbService(ILoggerFactory loggerFactory, IHostingEnvironment env, IOptions<SettingsModels> settings, IAmazonDynamoDB dynamoDbClient, IOptions<TableSettingsModel> table)
        {
            _settings = settings.Value;
            _logger = GetLogger(loggerFactory);
            _dynamoDbClient = dynamoDbClient;
            ProfileTables = new ConcurrentDictionary<string, Table>();
            _table = table.Value;
        }


        //  public async Task<Document> Get(string playerId, string gameSpace, string selector)
        //  {
        //      selector = ProcessSelector(selector);
        //      try
        //      {
        //          _logger.LogInformation($"Trying to fetch DynamoDb item with primary key: {playerId}");
        //          var tableName = gameSpace;
        //          //  var table = ProfileTables[tableName];
        //
        //          var request = new GetItemRequest
        //          {
        //              TableName = gameSpace,
        //              Key = new Dictionary<string, AttributeValue>() { { _table.KeyName, new AttributeValue { S = playerId } } },
        //              ProjectionExpression = selector,
        //              ConsistentRead = false,
        //              ReturnConsumedCapacity = ReturnConsumedCapacity.TOTAL
        //          };
        //          var response = await _dynamoDbClient.GetItemAsync(request);
        //          var responseItem = response.Item;
        //          var documentToReturn = Document.FromAttributeMap(responseItem);
        //
        //          return documentToReturn;
        //      }
        //      catch (AmazonDynamoDBException ex)
        //      {
        //          _logger.LogError($"Amazon error in GET table operation; Error: {ex.Message}");
        //          throw new AmazonOperationException(AmazonMessages.ErrorGetTableOpearation, ex);
        //      }
        //  }

        public async Task<List<Document>> GetPlayers(List<string> playerIds, string tableName, string selector)
        {
            selector = ProcessSelector(selector);
            try
            {
                _logger.LogInformation($"Trying to fetch DynamoDb item with primary keys count: {playerIds.Count}");
                var table = ProfileTables[tableName];
               
                var scan = new ScanRequest()
                {
                    TableName = tableName,
                    ProjectionExpression = selector,
                    ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                    {
                        { ":val", new AttributeValue{ BOOL = true} }
                    },
                    ExpressionAttributeNames = new Dictionary<string, string>
                    {
                         { "#pf", _table.DeletePlayerFlag}
                    },
                    FilterExpression = "#pf <> :val"
                };

                var scanRequest = await _dynamoDbClient.ScanAsync(scan);
                var mappedResultScan = new List<Document>();
                foreach (var item in scanRequest.Items)
                {
                    foreach (var el in item)
                    {
                        if (el.Key.Equals(_table.KeyName) && playerIds.Contains(el.Value.S))
                        {
                            mappedResultScan.Add(Document.FromAttributeMap(item));
                        }
                    }
                }
                return mappedResultScan;                
            }
            catch (AmazonDynamoDBException ex)
            {
                if (ex.ErrorCode.Equals("ValidationException"))
                {
                    _logger.LogError(ex.ToString());
                    throw new AmazonDynamoValidationException(ex.Message);
                }

                throw new AmazonOperationException(AmazonMessages.ErrorGetTableOpearation, ex);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Amazon error in GET table operation; Error: {ex.Message}");
                throw new AmazonOperationException(AmazonMessages.ErrorGetTableOpearation, ex);
            }
        }

        public async Task<List<Document>> GetPlayers(string tableName, ScanOperationConfig config)
        {
            try
            {
                var table = Table.LoadTable(_dynamoDbClient, tableName);
                Search search = table.Scan(config);

                List<Document> documentList = new List<Document>();
                //   documentList = await search.GetNextSetAsync();
                documentList = await search.GetRemainingAsync();

                return documentList;

            }
            catch (AmazonDynamoDBException ex)
            {
                if (ex.ErrorCode.Equals("ValidationException"))
                {
                    _logger.LogError(ex.ToString());
                    throw new AmazonDynamoValidationException(ex.Message);
                }

                throw new AmazonOperationException(AmazonMessages.ErrorGetTableOpearation, ex);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Amazon error in GET table operation; Error: {ex.Message}");
                throw new AmazonOperationException(AmazonMessages.ErrorGetTableOpearation, ex);
            }
        }

        public async Task<Document> CreatePlayer(string playerId, string jsonString, string tableName, string gameId)
        {
            try
            {
                _logger.LogInformation($"Trying to insert a new DynamoDb item with primary key: {playerId}");                       
              
                Document item = TrimInputJson(jsonString);
                item[_table.KeyName] = playerId;
                item[_table.DeletePlayerFlag] = false;
                item[_table.GameId] = gameId;               

                var request = new PutItemRequest()
                {
                    Item = item.ToAttributeMap(),
                    TableName = tableName
                };

                var resp = await _dynamoDbClient.PutItemAsync(request);
                return item;
            }
            catch (AmazonDynamoDBException ex)
            {
                _logger.LogError($"Amazon error in CREATE Item operation; Error: {ex.Message}");
                throw new AmazonOperationException(AmazonMessages.ErrorCreateItemOpearation, ex);
            }
        }

        public UpdateDocument ProcessUpdate(string playerId, string item, string tableName)
        {
            _logger.LogInformation($"Trying to update a DynamoDb item with primary key: {playerId}");

            var updateDocument = new UpdateDocument()
            {
                GameSpace = tableName,
                Document = item
            };

            return updateDocument;
        }

        public async Task<Document> UpdateWithConfiguration(UpdateDocument updateDocument)
        {          
            var item = TrimInputJson(updateDocument.Document);

            if (!item.Keys.Contains(_table.KeyName))
            {
                throw new MissingRequiredParameterException("All Json Objects should contain playerId");
            }

            var playerId = item[_table.KeyName];

            _logger.LogInformation($"Trying to UpdateConfiguration for playerId: {playerId}");

            try
            {
                var updateConfig = new UpdateItemOperationConfig()
                {
                    ConditionalExpression = CreateExpression(playerId)
                };

                // We need to load the database here, because there's no endpoint that will load it if there's something in the queue
                SetProfilesTable(updateDocument.GameSpace);

                //  var tableName = BuildTableName(updateDocument.GameSpace);
                var tableName = updateDocument.GameSpace;
                var table = ProfileTables[tableName];

                await table.UpdateItemAsync(item, updateConfig);
                return item;
            }
            catch (ConditionalCheckFailedException ex)
            {
                _logger.LogError(ex.ToString());
                throw new ItemNotFoundException(string.Format(Messages.ItemNotFound, "Profile", item[_table.KeyName]));
            }
            catch (AmazonDynamoDBException ex)
            {
                _logger.LogError($"Amazon error in UPDATE Item operation; Error: {ex.Message}");
                throw new AmazonOperationException(AmazonMessages.ErrorUpdateItemOpearation, ex);
            }
        }

        public void SetProfilesTable(string tableName)
        {
            try
            {
                //var tableName = BuildTableName(gameSpace);
                //var tableName = gameSpace;              

                if (!ProfileTables.ContainsKey(tableName))
                {
                    Table currentTable = Table.LoadTable(_dynamoDbClient, tableName);
                    ProfileTables.TryAdd(tableName, currentTable);
                }
            }
            catch (AmazonDynamoDBException ex)
            {
                _logger.LogError($"Amazon error in LoadTable operation; Error: {ex}");
                throw new AmazonOperationException(AmazonMessages.ErrorGetTableOpearation, ex);
            }
        }

        public string BuildTableName(string gameId, string gameName)
        {            
            // return string.Join(".", _settings.DynamoDbTablePrefix, _settings.Environment, gameSpace);
            var name = new StringBuilder();
            name.Append(gameId);
            name.Append(".");
            name.Append(gameName);
            name.Append(".playerData");

            return name.ToString();
        }

        private string ProcessSelector(string selector)
        {
            if (string.IsNullOrEmpty(selector) || string.IsNullOrWhiteSpace(selector))
            {
                return null;
            }

            var splittedSelector = selector.Split(',');
            if (splittedSelector.Count() < 1)
            {
                return null;
            }
            if (!splittedSelector.Contains(_table.KeyName))
            {
                selector += ($", {_table.KeyName}");
            }

            return selector;
        }

        private Expression CreateExpression(string playerId)
        {
            var updateExpression = "#acc =:val";
            var expressionAttributeNames = new Dictionary<string, string>
                {
                    { "#acc", _table.KeyName },
                    { "#pf", _table.DeletePlayerFlag}
                };
            var expressionAttributeValues = new Dictionary<string, DynamoDBEntry>
                {
                    { ":val", playerId },
                    {":att", false }
                };
            var expression = new Expression()
            {
                ExpressionAttributeNames = expressionAttributeNames,
                ExpressionAttributeValues = expressionAttributeValues,
                ExpressionStatement = updateExpression
            };
            expression.ExpressionStatement = "(#acc =:val) and (#pf = :att)";
            return expression;
        }

        public Document TrimInputJson(string jsonString)
        {
            var arrayFromJsonString = jsonString.Split("\"");
            var trimedData = new StringBuilder();
            var item = new Document();

            if (jsonString == "{}")
            {
                item = Document.FromJson(jsonString.ToString());
            }
            else
            {
                for (int i = 0; i < arrayFromJsonString.Length; i++)
                {
                    if (i == 0)
                    {
                        trimedData.Append("{");
                    }
                    else
                    {
                        trimedData.Append("\"");
                        trimedData.Append(arrayFromJsonString[i].Trim());
                    }
                }

                item = Document.FromJson(trimedData.ToString());
            }
            return item;
        }

        public async Task<string> DeletePlayer(string tableName, string playerId)
        {
            _logger.LogInformation($"Prepare request to delete player with name: {playerId}");
            try
            {
                var document = new Document();
                document[_table.KeyName] = playerId;
                document[_table.DeletePlayerFlag] = true;
                var doc = ProcessUpdate(playerId, document.ToJsonPretty(), tableName);

                var res = await UpdateWithConfiguration(doc);
                if (res.Keys.Count != 0)
                {
                    return $"Player {playerId} is deleted";
                }

                if (string.IsNullOrEmpty(res))
                {
                    //return null if the required value is not found
                    return null;
                }

                return $"Player {playerId} is deleted";
            }

            catch (ItemNotFoundException ex)
            {
                throw new ItemNotFoundException(ex.Message);
            }
            catch (ConditionalCheckFailedException ex)
            {
                throw new ItemNotFoundException(ex.Message);
            }
            catch (AmazonDynamoDBException ex)
            {
                throw new AmazonDynamoDBException(ex.Message);
            }
        }
    }
}
