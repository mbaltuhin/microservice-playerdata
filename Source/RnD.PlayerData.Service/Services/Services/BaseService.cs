using Microsoft.Extensions.Logging;

namespace RnD.PlayerData.Services.Services
{
    public class BaseService
    {
        protected ILogger GetLogger(ILoggerFactory loggerFactory)
        {
            return loggerFactory.CreateLogger(GetType().Namespace);
        }
    }
}
