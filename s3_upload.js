const fs = require('fs');
const yargs = require('yargs');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();

const bucketOptions = {
    describe: 'Name of the S3 bucket',
    demand: true,
    alias: 'b'
};
const fileOptions = {
    describe: 'Filename of the S3 object',
    demand: true,
    alias: 'f'
};
const argv = yargs
    .command('upload', 'Upload file to S3', {
        bucket: bucketOptions,
        file: fileOptions
    })
    .help()
    .argv;
var command = argv._[0];

// S3 upload
if (command === 'upload') {
    fs.readFile(argv.file, function (err, data) {
        if (err) { throw err; }
        params = { Bucket: argv.bucket, Key: argv.file, Body: data };
    
        s3.putObject(params, function (err, data) {
            if (err) {
                console.log(err)
            } else {
                console.log(`Successfully uploaded data to ${argv.bucket}/${argv.file}`);
            }
        });
    });
}

