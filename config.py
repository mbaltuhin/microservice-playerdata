import getopt
import os
import json
import sys

# Opening AppSettings.json
valid_issuer = ''
env = ''
queue = ''
cdnAcc = ''
enableSwagger = ''
os.system("pkill dnsmasq")
os.system("dnsmasq")

options, remainder = getopt.gnu_getopt(
    sys.argv[1:], 'o:v', ['issuer-url=', 'issuer-url-protocol=', 'game-server-environment=', "queue-name=", "account-id=", "enable-swagger="])
for opt, arg in options:
    if opt in '--issuer-url':
        valid_issuer = arg
    elif opt in '--issuer-url-protocol':
        issuer_url_protocol = arg
    elif opt == '--game-server-environment':
        env = arg
    elif opt == '--queue-name':
        queue = arg
    elif opt == '--account-id':
        cdnAcc = arg
    elif opt == '--enable-swagger':
        enableSwagger = arg

file = open('/app/Source/RnD.PlayerData.Service/API/appsettings.json', 'r', encoding='utf-8-sig')
tmp_file = ''
for line in file:
    if r'//"' not in line:
        tmp_file += line
open('tmp_file.json', 'w').write(tmp_file)
json_file = json.load(open("tmp_file.json", 'r', encoding="utf-8-sig"))
os.system('rm ./tmp_file.json')

# File Properties

json_file['ResourceStrings']['ValidIssuer'] = valid_issuer
json_file['rnd']['Environment'] = env
json_file['Worker']['QueueName'] = queue
json_file['rnd']['PlayerId'] = cdnAcc
json_file['Swagger']['EnableSwagger'] = enableSwagger

# Changing values

# Set new values here...

# Writing AppSettings.json
with open('/app/Source/RnD.PlayerData.Service/API/appsettings.json', 'w') as out_file:
    json.dump(json_file, out_file, indent=4)

# Starting .NET Service
os.system('python3 /app/getregion.py && cd /app/Source/RnD.PlayerData.Service/API/ && dotnet run -c Release')
